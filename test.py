from flask import Flask,render_template,request,redirect,session,flash,url_for

from functools import wraps

from flask_mysqldb import MySQL

# connecting to the database

app=Flask(__name__)
app.config['MYSQL_HOST']='localhost'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='siddhu'
app.config['MYSQL_DB']='vaichar'
app.config['MYSQL_CURSORCLASS']='DictCursor'
mysql=MySQL(app)

@app.route("/")
#Login
@app.route('/login',methods=['POST','GET'])
#This function is used to login if all the credentials are right or else it throws an error
def login():
    status=True
    if request.method=='POST':  
        email=request.form["email"]
        pwd=request.form["upass"]
        cur=mysql.connection.cursor()
        cur.execute("select * from users where EMAIL=%s and UPASS=%s",(email,pwd))
        data=cur.fetchone()
        if data:
            session['logged_in']=True
            session['username']=data["UNAME"]
            flash('Login Successfully','success')
            return redirect('home')
        else:
            return {'message':"Invalid Login creds, Try again"},404
    return render_template("login.html")

#This function is used to check if the user is logged in or not
def is_logged_in(f):
    # 
	@wraps(f)
	def wrap(*args,**kwargs):
		if 'logged_in' in session:
			return f(*args,**kwargs)
		else:
			flash('Unauthorized, Please Login','danger')
			return redirect(url_for('login'))
	return wrap

#This function is used to register the user creds 
@app.route('/reg',methods=['POST','GET'])
def reg():
    status=False
    if request.method=='POST':
        name=request.form["uname"]
        email=request.form["email"]
        pwd=request.form["upass"]
        cur=mysql.connection.cursor()
        cur.execute("insert into users(UNAME,UPASS,EMAIL) values(%s,%s,%s)",(name,pwd,email))
        mysql.connection.commit()
        cur.close()
        flash('Registration Successfully. Login Here...','success')
        return redirect('login')
    return render_template("reg.html",status=status)

#Home page
@app.route("/home")

def home():
    if 'logged_in' in session:
        return render_template('home.html')
    else:
        return {'message':"Login First"},401

@app.route("/contactus")
#this function helps users to clear their queries
def contactus():
    if 'logged_in' in session:
        return render_template('contact.html')
    else:
        return {'message':"Login First"},401
    
#logout
@app.route("/logout")

def logout():
	session.clear()
	flash('You are now logged out','success')
	return redirect(url_for('login'))

@app.route("/questions")
#this function is used to see all the queries 
def questions():
    if 'logged_in' in session:
        return render_template('questions.html')
    else:
        return {'message':"Login First"},401

@app.route("/profile/<id>")
#this function gives the description of user
def profile(id):
    if 'logged_in' in session:
        cur=mysql.connection.cursor()
        cur.execute("select * from Persons where PERSONID=%s",(id))
        data=dict(cur.fetchone())
        return data
    else:
        return {'message':"Login First"},401


@app.route("/delete",methods=['GET','POST'])
#this function deletes the specified user
def delete():
    status=False
    if request.method=='POST':
        id=request.form["id"]
        cur=mysql.connection.cursor()
        cur.execute("delete from users where UID=%s",(id))
        mysql.connection.commit()
        cur.close()
        return redirect('home')
    return render_template("delete.html",status=status)

    # if 'logged_in' in session:
    #     return render_template('delete.html')
    # else:
    #     return {'message':"Login First"},401


@app.route("/company/careers")
#this function is used to see all the queries 
def company_carrers():
    if 'logged_in' in session:
        return render_template('careers.html')
    else:
        return {'message':"Login First"},401

@app.route("/work-here")
#this function is used to see all the queries 
def work_here():
    if 'logged_in' in session:
        return render_template('workhere.html')
    else:
        return {'message':"Login First"},401

#login-taking the parameters(email,pwd) in the url(path parameters)
@app.route('/another_login/<string:email>/<string:pwd>')
def new_login(email,pwd):
# this function is used to check the credentials and login or else it throws an error
	cursor = mysql.connection.cursor()  #url:http://127.0.0.1:50012/another_login/email/pwd

	cursor.execute(f'SELECT * FROM users WHERE EMAIL=%s and UPASS=%s"',(email,pwd))

	account = cursor.fetchone()

	if account:                                          
		session['logged_in'] = True
		return render_template('home.html')

	return render_template('login.html')

# login using postman
@app.route("/login_postman",methods=["POST"])

def login_postman(email,pwd):
# this function is used to check the credentials and login or else it throws an error

	data=request.json
	email=data['email']
	pwd=data['pass']
	cursor = mysql.connection.cursor() 
	cursor.execute(f'SELECT * FROM users WHERE EMAIL=%s and UPASS=%s"',(email,pwd))
	account = cursor.fetchone()
	if account:                                        
		session['logged_in'] = True
		return render_template('home.html')
	return render_template('login.html')


#driver code
if __name__=='__main__':
    app.secret_key='secret123'
    app.run(debug=True,port=50012)
